import axios from 'axios'

const COMMENTS_URL = 'https://jsonplaceholder.typicode.com/comments'

interface Emails {
  id: number
  email: string
}

const getData = (url: string): Promise<Emails[]> => {
  return axios.get<Emails[]>(url)
    .then(response => response.data);
}

getData(COMMENTS_URL)
  .then(data => {
    data.forEach(item => {
      console.log(`ID: ${item.id}, Email: ${item.email}`)
    })
  })

/**
 * ID: 1, Email: Eliseo...
 * ID: 2, Email: Jayne_Kuhic...
 * ID: 3, Email: Nikita...
 * ID: 4, Email: Lew...
 * ...
 */